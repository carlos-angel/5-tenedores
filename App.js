import { LogBox } from 'react-native';
import Toast from 'react-native-toast-message';
import Navigation from 'navigations/Navigation';
import * as firebase from 'utils/firabase';

LogBox.ignoreLogs(['Setting a timer', 'AsyncStorage']);

export default function App() {
  return (
    <>
      <Navigation />
      <Toast />
    </>
  );
}
