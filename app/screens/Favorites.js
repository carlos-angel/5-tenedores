import { useState, useCallback, useRef } from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { Icon } from 'react-native-elements';
import { useFocusEffect } from '@react-navigation/native';
import { useAuthenticated } from 'hooks/useAuthenticated';
import { getFavoritesRestaurants } from 'services/restaurant/favorite-restaurant.service';
import LoadingModal from 'components/Shared/LoadingModal';
import Loading from 'components/Shared/Loading';
import FavoriteRestaurantItem from 'components/Restaurant/FavoriteRestaurantItem';
import AccessDenied from 'components/Security/AccessDenied';

export default function Favorites({ navigation }) {
  const [restaurants, setRestaurants] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const { isAuth } = useAuthenticated();

  useFocusEffect(
    useCallback(() => {
      if (isAuth) {
        getFavoritesRestaurants()
          .then((data) => setRestaurants(data))
          .catch(() => setRestaurants([]));
      }
    }, [isAuth, isLoading]),
  );

  if (!isAuth)
    return (
      <AccessDenied
        navigation={navigation}
        message='Esta sección es solo para usuarios registrados'
      />
    );

  if (restaurants?.length === 0)
    return (
      <View style={styles.viewNotFavoritesRestaurants}>
        <Icon type='material-community' name='alert-outline' size={50} />
        <Text styles={styles.textNotFavoritesRestaurants}>
          No tienes ningún restaurante favorito
        </Text>
      </View>
    );

  return (
    <View style={styles.viewBody}>
      {restaurants ? (
        <FlatList
          data={restaurants}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => (
            <FavoriteRestaurantItem {...item} navigation={navigation} setIsLoading={setIsLoading} />
          )}
        />
      ) : (
        <Loading message='Cargando Favoritos' />
      )}
      <LoadingModal text='eliminando restaurante' isVisible={isLoading} />
    </View>
  );
}

const styles = StyleSheet.create({
  viewNotFavoritesRestaurants: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNotFavoritesRestaurants: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  viewBody: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  loaderRestaurants: {
    marginTop: 10,
    marginBottom: 10,
  },
});
