import UserLogged from 'screens/Account/UserLogged';
import UserGuest from 'screens/Account/UserGuest';
import LoadingModal from 'components/Shared/LoadingModal';
import { useAuthenticated } from 'hooks/useAuthenticated';

export default function Account() {
  const { isAuth, loading } = useAuthenticated();

  if (loading) return <LoadingModal isVisible={loading} text='Cargando...' />;

  return isAuth ? <UserLogged /> : <UserGuest />;
}
