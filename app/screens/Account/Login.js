import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Formik } from 'formik';
import * as Yup from 'yup';
import Toast from 'react-native-toast-message';
import { onAuthStateChanged, getAuth } from 'firebase/auth';
import { signInService } from 'services/auth/signIn.service';
import colors from 'constants/colors';

export default function Login() {
  const [showPassword, setShowPassword] = useState(false);
  const { navigate, goBack } = useNavigation();

  const auth = getAuth();
  useEffect(() => {
    onAuthStateChanged(auth, (user) => user && navigate('account'));
  }, [auth.currentUser]);

  return (
    <KeyboardAwareScrollView>
      <Image
        source={require('assets/img/5-tenedores-letras-icono-logo.png')}
        resizeMode='contain'
        style={styles.logo}
      />
      <View style={styles.viewContainer}>
        <Text style={styles.textRegister}>
          ¿Aún no tienes una cuenta?{' '}
          <Text style={styles.buttonRegister} onPress={() => navigate('register')}>
            Crea tu cuenta Aquí
          </Text>
        </Text>
      </View>
      <View style={styles.viewForm}>
        <View style={styles.formContainer}>
          <Formik
            initialValues={{ email: '', password: '' }}
            validationSchema={Yup.object({
              email: Yup.string()
                .email('Ingrese un formato de email valido')
                .required('Ingrese su email'),
              password: Yup.string()
                .min(8, 'La contraseña debe tener mínimo 8 caracteres')
                .required('Ingrese una contraseña'),
            })}
            onSubmit={async (values) => {
              const result = await signInService(values);

              result.error && Toast.show({ type: 'error', text1: result.message });
              !result.error && goBack();
            }}
          >
            {({ handleChange, handleSubmit, setFieldTouched, errors, isSubmitting, touched }) => (
              <>
                <Input
                  placeholder='Correo electrónico'
                  containerStyle={styles.inputForm}
                  rightIcon={
                    <Icon type='material-community' name='at' iconStyle={styles.iconRight} />
                  }
                  onChangeText={handleChange('email')}
                  onBlur={() => setFieldTouched('email', true, true)}
                  errorMessage={touched.email && errors.email ? errors.email : ''}
                />
                <Input
                  placeholder='Contraseña'
                  containerStyle={styles.inputForm}
                  password={true}
                  secureTextEntry={!showPassword}
                  rightIcon={
                    <Icon
                      type='material-community'
                      name={showPassword ? 'eye-off-outline' : 'eye-outline'}
                      iconStyle={styles.iconRight}
                      onPress={() => setShowPassword(!showPassword)}
                    />
                  }
                  onBlur={() => setFieldTouched('password', true, true)}
                  onChangeText={handleChange('password')}
                  errorMessage={touched.password && errors.password ? errors.password : ''}
                />

                <Button
                  title='Iniciar sesión'
                  onPress={handleSubmit}
                  containerStyle={styles.buttonContainerLogin}
                  buttonStyle={styles.buttonLogin}
                  loading={isSubmitting}
                  disabled={isSubmitting}
                />
              </>
            )}
          </Formik>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: '100%',
    height: 150,
    marginTop: 20,
  },
  viewContainer: {
    marginRight: 40,
    marginLeft: 40,
  },
  textRegister: {
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
  },
  buttonRegister: {
    color: '#006a80',
    fontWeight: 'bold',
  },
  divider: {
    backgroundColor: colors.PRIMARY_COLOR,
    margin: 40,
  },
  viewForm: {
    marginRight: 40,
    marginLeft: 40,
  },
  formContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  inputForm: {
    width: '100%',
    marginTop: 20,
  },
  buttonContainerLogin: {
    marginTop: 20,
    width: '95%',
  },
  buttonLogin: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
  iconRight: {
    color: '#c1c1c1',
  },
});
