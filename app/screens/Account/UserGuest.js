import React from 'react';
import { ScrollView, View, StyleSheet, Text, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import colors from 'constants/colors';

export default function UserGuest() {
  const { navigate } = useNavigation();

  return (
    <ScrollView centerContent={true} style={styles.viewBody}>
      <Image
        source={require('assets/img/user-guest.jpg')}
        resizeMode='contain'
        style={styles.image}
      />
      <Text style={styles.title}>Consulta tu perfil de 5 Tenedores</Text>
      <Text style={styles.description}>
        ¿Como describirías tu mejor restaurante? Busca y visualiza los mejores restaurantes de una
        forma sencilla, vota y evaluá cuál te ha gustado más y comenta como ha sido tu experiencia.
      </Text>
      <View style={styles.viewButton}>
        <Button
          buttonStyle={styles.button}
          containerStyle={styles.buttonContainer}
          title='Ver tu perfil'
          onPress={() => navigate('login')}
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    marginLeft: 30,
    marginRight: 30,
  },
  image: {
    height: 300,
    width: '100%',
    marginBottom: 40,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 19,
    marginBottom: 10,
    textAlign: 'center',
  },
  description: {
    textAlign: 'center',
    marginBottom: 20,
  },
  viewButton: {
    flex: 1,
    alignItems: 'center',
  },
  button: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
  buttonContainer: {
    width: '70%',
  },
});
