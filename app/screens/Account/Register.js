import React, { useState, useRef } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Input, Icon, Button } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useNavigation } from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import { registerService } from 'services/auth/register.service';
import colors from 'constants/colors';

export default function Register() {
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const { navigate } = useNavigation();

  return (
    <KeyboardAwareScrollView>
      <View>
        <Image
          source={require('assets/img/5-tenedores-letras-icono-logo.png')}
          resizeMode='contain'
          style={styles.logo}
        />

        <View style={styles.viewForm}>
          <View style={styles.formContainer}>
            <Formik
              initialValues={{ email: '', password: '', confirmPassword: '' }}
              validationSchema={Yup.object({
                email: Yup.string()
                  .email('Ingrese un formato de email valido')
                  .required('Ingrese su email'),
                password: Yup.string()
                  .min(8, 'La contraseña debe tener mínimo 8 caracteres')
                  .required('Ingrese una contraseña'),
                confirmPassword: Yup.string()
                  .oneOf([Yup.ref('password')], 'Las contraseñas no son iguales')
                  .required('Confirme su contraseña'),
              })}
              onSubmit={async (values) => {
                const { email, password } = values;
                const result = await registerService({ email, password });

                if (result.error) {
                  Toast.show({ type: 'error', text1: result.message });
                } else {
                  navigate('account');
                }
              }}
            >
              {({ handleChange, handleSubmit, setFieldTouched, errors, isSubmitting, touched }) => (
                <>
                  <Input
                    placeholder='Correo electrónico'
                    containerStyle={styles.inputForm}
                    rightIcon={
                      <Icon type='material-community' name='at' iconStyle={styles.iconRight} />
                    }
                    onChangeText={handleChange('email')}
                    onBlur={() => setFieldTouched('email', true, true)}
                    errorMessage={touched.email && errors.email ? errors.email : ''}
                  />
                  <Input
                    placeholder='Contraseña'
                    containerStyle={styles.inputForm}
                    password={true}
                    secureTextEntry={!showPassword}
                    rightIcon={
                      <Icon
                        type='material-community'
                        name={showPassword ? 'eye-off-outline' : 'eye-outline'}
                        iconStyle={styles.iconRight}
                        onPress={() => setShowPassword(!showPassword)}
                      />
                    }
                    onBlur={() => setFieldTouched('password', true, true)}
                    onChangeText={handleChange('password')}
                    errorMessage={touched.password && errors.password ? errors.password : ''}
                  />
                  <Input
                    placeholder='Confirmar contraseña'
                    containerStyle={styles.inputForm}
                    password={true}
                    secureTextEntry={!showConfirmPassword}
                    rightIcon={
                      <Icon
                        type='material-community'
                        name={showConfirmPassword ? 'eye-off-outline' : 'eye-outline'}
                        iconStyle={styles.iconRight}
                        onPress={() => setShowConfirmPassword(!showConfirmPassword)}
                      />
                    }
                    onBlur={() => setFieldTouched('confirmPassword', true, true)}
                    onChangeText={handleChange('confirmPassword')}
                    errorMessage={
                      touched.confirmPassword && errors.confirmPassword
                        ? errors.confirmPassword
                        : ''
                    }
                  />

                  <Button
                    title='Crear cuenta'
                    onPress={handleSubmit}
                    containerStyle={styles.buttonContainerRegister}
                    buttonStyle={styles.buttonRegister}
                    loading={isSubmitting}
                    disabled={isSubmitting}
                  />
                </>
              )}
            </Formik>
          </View>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: '100%',
    height: 150,
    marginTop: 20,
    marginBottom: 20,
  },
  viewForm: {
    marginRight: 40,
    marginLeft: 40,
  },
  formContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  inputForm: {
    width: '100%',
    marginTop: 20,
  },
  buttonContainerRegister: {
    marginTop: 20,
    width: '95%',
  },
  buttonRegister: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
  iconRight: {
    color: '#c1c1c1',
  },
});
