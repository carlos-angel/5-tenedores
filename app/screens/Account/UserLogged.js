import { useRef, useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import LoadingModal from 'components/Shared/LoadingModal';
import { signOutService } from 'services/auth/signOut.service';
import InformationUser from 'components/InformationUser';
import AccountOptions from 'components/AccountOptions';
import { Provider } from 'context/account.context';
import { informationUserService } from 'services/account/information-user.service';
import colors from 'constants/colors';

export default function UserLogged() {
  const [loading, setLoading] = useState({ text: '', isVisible: false });
  const [user, setUser] = useState(null);
  const [reloadUser, setReloadUser] = useState(false);

  useEffect(() => {
    const { data, error } = informationUserService();
    if (!error) setUser(data);
    setReloadUser(false);
  }, [reloadUser]);

  return (
    <Provider value={{ user, setUser, setReloadUser, setLoading }}>
      <View style='styles.viewUserLogged'>
        <InformationUser />
        <AccountOptions />
        <Button
          buttonStyle={styles.buttonCloseSession}
          titleStyle={styles.buttonCloseSessionText}
          onPress={signOutService}
          title='cerrar sesión'
        />
        <LoadingModal text={loading.text} isVisible={loading.isVisible} />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  viewUserLogged: {
    minHeight: '100%',
    backgroundColor: '#f2f2f2',
  },
  buttonCloseSession: {
    paddingTop: 20,
    borderRadius: 0,
    backgroundColor: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
    paddingBottom: 20,
  },
  buttonCloseSessionText: {
    color: colors.PRIMARY_COLOR,
  },
});
