import { useState, useEffect, useRef } from 'react';
import { View, StyleSheet, FlatList, Image } from 'react-native';
import { SearchBar } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import SearchRestaurant from 'components/Restaurant/SearchRestaurant';
import { getRestaurantsByQuerySearch } from 'services/restaurant/get-restaurants-by-query-search';

export default function Search({ navigation }) {
  const [search, setSearch] = useState('');
  const [restaurants, setRestaurants] = useState([]);

  const isNoResults = !search || restaurants.length === 0;

  useEffect(() => {
    if (search) {
      getRestaurantsByQuerySearch(search)
        .then((data) => setRestaurants(data))
        .catch(() => Toast.show({ type: 'error', text1: 'No se encontraron restaurantes' }));
    }
  }, [search]);

  return (
    <View>
      <SearchBar
        placeholder='Buscar restaurante'
        onChangeText={(e) => setSearch(e)}
        value={search}
        containerStyle={styles.searchBar}
      />
      {isNoResults ? (
        <View style={styles.viewNoResultFound}>
          <Image source={require('assets/no-result-found.png')} style={styles.imageNoResultFound} />
        </View>
      ) : (
        <FlatList
          data={restaurants}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => <SearchRestaurant {...item} navigation={navigation} />}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  searchBar: { marginBottom: 20 },
  viewNoResultFound: { flex: 1, alignItems: 'center' },
  imageNoResultFound: { width: 200, height: 200 },
});
