import { useState, useEffect, useRef } from 'react';
import { View, FlatList } from 'react-native';
import Toast from 'react-native-toast-message';
import Loading from 'components/Shared/Loading';
import { getTopRestaurants } from 'services/restaurant/get-top-restaurants';
import TopRestaurantItem from 'components/Restaurant/TopRestaurantItem';

export default function TopRestaurants({ navigation }) {
  const [topRestaurants, setTopRestaurants] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getTopRestaurants(5)
      .then((data) => setTopRestaurants(data))
      .catch(() => Toast.show({ type: 'error', text1: 'No se encontraron restaurantes' }))
      .finally(() => setLoading(false));
  }, []);

  if (loading) return <Loading message='cargando restaurantes' />;

  return (
    <View>
      <FlatList
        data={topRestaurants}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => <TopRestaurantItem {...item} navigation={navigation} />}
      />
    </View>
  );
}
