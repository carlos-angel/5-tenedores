import { ScrollView, StyleSheet, Dimensions, View, Text } from 'react-native';
import { useState, useEffect, useCallback } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { Rating, ListItem, Icon } from 'react-native-elements';
import MapView, { Marker } from 'react-native-maps';
import openMap from 'react-native-open-maps';
import { map } from 'lodash';
import { getRestaurantsById } from 'services/restaurant/get-restaurant.service';
import LoadingModal from 'components/Shared/LoadingModal';
import Carousel from 'components/Shared/Carousel';
import Reviews from 'components/Review/Reviews';
import Favorite from 'components/Shared/Favorite';
import { useAuthenticated } from 'hooks/useAuthenticated';
import ButtonReview from 'components/Review/ButtonReview';
import colors from 'constants/colors';
import Location from 'components/Restaurant/Location';
import Information from 'components/Restaurant/Information';

const screenWidth = Dimensions.get('window').width;

export default function Restaurant({ navigation, route }) {
  const { isAuth } = useAuthenticated();
  const [restaurant, setRestaurant] = useState(null);
  const { id, name } = route.params;
  const loading = !restaurant;

  useEffect(() => {
    navigation.setOptions({ title: name });
  }, []);

  useFocusEffect(
    useCallback(() => {
      getRestaurantsById(id)
        .then((data) => (data ? setRestaurant(data) : navigation.goBack()))
        .catch(() => navigation.goBack());
    }, [id]),
  );

  if (loading) return <LoadingModal isVisible={loading} text='cargando Restaurante' />;

  const { description, images, location, address, rating } = restaurant;
  const { latitude, longitude } = location;

  const moreInfo = [
    {
      text: address,
      iconName: 'map-marker',
      iconType: 'material-community',
      action: null,
    },
  ];

  return (
    <ScrollView vertical style={styles.viewBody}>
      {isAuth && <Favorite idRestaurant={id} sizeIcon={35} />}

      <Carousel images={images} height={250} width={screenWidth} />

      <Information>
        <Information.Title>
          <Text style={styles.nameRestaurant}>{name}</Text>
          <Rating imageSize={20} readonly startingValue={parseFloat(rating)} />
        </Information.Title>
        <Information.Description description={description} />
      </Information>

      <Location title='Ubicación del restaurante'>
        <MapView
          style={{ height: 120, width: '100%' }}
          initialRegion={location}
          onPress={() => openMap({ latitude, longitude, zoom: 19, query: name })}
        >
          <Marker coordinate={{ latitude, longitude }} />
        </MapView>
      </Location>

      {map(moreInfo, (info, index) => (
        <ListItem key={index} style={styles.containerListItem}>
          <Icon color={colors.PRIMARY_COLOR} type={info.iconType} name={info.iconName} />
          <ListItem.Content>
            <ListItem.Title>{info.text}</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      ))}

      <ButtonReview navigation={navigation} idRestaurant={id} />
      <Reviews idRestaurant={id} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: colors.BACKGROUND,
  },
  nameRestaurant: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.TERTIARY_COLOR,
  },
  containerListItem: {
    borderBottomColor: '#dedede',
    borderBottomWidth: 1,
  },
});
