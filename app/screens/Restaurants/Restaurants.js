import { useState, useCallback, useRef } from 'react';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { useAuthenticated } from 'hooks/useAuthenticated';
import { View, Text, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import { Icon } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { size } from 'lodash';
import { getRestaurantsByLimit } from 'services/restaurant/get-restaurants.service';
import RestaurantItem from 'components/Restaurant/RestaurantItem';
import Loading from 'components/Shared/Loading';
import colors from 'constants/colors';

export default function Restaurants() {
  const { isAuth } = useAuthenticated();
  const navigation = useNavigation();
  const [restaurants, setRestaurants] = useState([]);
  const [loading, setLoading] = useState(true);

  const limitRestaurants = 10;
  const sizeRestaurants = size(restaurants);

  useFocusEffect(
    useCallback(() => {
      (async () => await moreRestaurants())();
    }, []),
  );

  const moreRestaurants = () => {
    const startAfterCreateAt = sizeRestaurants > 0 ? restaurants[sizeRestaurants - 1].createAt : '';

    getRestaurantsByLimit({ startAfterCreateAt, limitRestaurants })
      .then((newRestaurants) => {
        setRestaurants([...restaurants, ...newRestaurants]);
        size(newRestaurants) < 1 && setLoading(false);
      })
      .catch(() => Toast.show({ type: 'error', text1: 'No se encontraron restaurantes' }));
  };

  return (
    <View style={styles.viewBody}>
      {sizeRestaurants > 0 ? (
        <FlatList
          data={restaurants}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => <RestaurantItem {...item} navigation={navigation} />}
          onEndReached={loading && moreRestaurants}
          onEndReachedThreshold={0.5}
          ListFooterComponent={
            loading ? (
              <View style={styles.loading}>
                <ActivityIndicator size='large' color={colors.PRIMARY_COLOR} />
              </View>
            ) : (
              <View style={styles.notMoreRestaurants}>
                <Text style={{ color: '#9CA3AF' }}>No hay más restaurantes</Text>
              </View>
            )
          }
        />
      ) : (
        <Loading message='Cargando Restaurantes' />
      )}

      {isAuth && (
        <Icon
          type='material-community'
          name='plus'
          color={colors.PRIMARY_COLOR}
          reverse
          containerStyle={styles.buttonPlusContainer}
          onPress={() => navigation.navigate('add-restaurant')}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
    backgroundColor: colors.BACKGROUND,
  },
  buttonPlusContainer: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    shadowColor: 'black',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.5,
  },
  notMoreRestaurants: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: 'center',
  },
});
