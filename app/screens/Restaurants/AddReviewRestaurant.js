import { View, StyleSheet } from 'react-native';
import { AirbnbRating, Button } from 'react-native-elements';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { useState } from 'react';
import { getAuth } from 'firebase/auth';
import Input from 'components/Form/Input';
import { addReviewRestaurantService } from 'services/reviews/add-review-restaurant';
import Toast from 'react-native-toast-message';
import { updateRatingRestaurant } from 'services/restaurant/update-rating-restaurant';
import colors from 'constants/colors';

export default function AddReviewRestaurant({ navigation, route }) {
  const [rating, setRating] = useState(null);
  const { idRestaurant } = route.params;

  return (
    <View style={styles.viewBody}>
      <View style={styles.viewRating}>
        <AirbnbRating
          count={5}
          reviews={['Pésimo', 'Deficiente', 'Normal', 'Muy Bueno', 'Excelente']}
          defaultRating={0}
          size={35}
          onFinishRating={(value) => setRating(value)}
        />
      </View>
      <View style={styles.viewForm}>
        <Formik
          initialValues={{ title: '', review: '' }}
          validationSchema={Yup.object({
            title: Yup.string().required('El comentario debe tener un titulo'),
            review: Yup.string().required('El comentario no puede estar vacío'),
          })}
          onSubmit={async (values) => {
            if (!rating) {
              Toast.show({
                type: 'error',
                text1: 'Necesita calificar el restaurante para publicar un comentario',
              });
              return;
            }

            const { uid, photoURL } = getAuth().currentUser;
            const payload = {
              idUser: uid,
              avatar: photoURL,
              idRestaurant: idRestaurant,
              rating,
              ...values,
            };

            const { error, message } = await addReviewRestaurantService(payload);
            !error && (await updateRatingRestaurant(idRestaurant, rating));
            error && Toast.show({ type: 'error', text1: message });
            !error && navigation.goBack();
          }}
        >
          {({ handleChange, handleSubmit, setFieldTouched, errors, isSubmitting, touched }) => (
            <>
              <Input
                placeholder='Titulo'
                containerStyle={styles.input}
                onChangeText={handleChange('title')}
                onBlur={() => setFieldTouched('title', true, true)}
                errorMessage={touched.title && errors.title ? errors.title : ''}
              />

              <Input
                placeholder='Comentario'
                multiline={true}
                inputContainerStyle={styles.textArea}
                onChangeText={handleChange('review')}
                onBlur={() => setFieldTouched('review', true, true)}
                errorMessage={touched.review && errors.review ? errors.review : ''}
              />

              <Button
                title='Enviar comentario'
                onPress={handleSubmit}
                containerStyle={styles.buttonContainer}
                buttonStyle={styles.buttonSend}
                loading={isSubmitting}
                disabled={isSubmitting}
              />
            </>
          )}
        </Formik>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: {
    flex: 1,
  },
  viewRating: {
    height: 110,
    backgroundColor: '#f2f2f2',
  },
  viewForm: {
    flex: 1,
    alignItems: 'center',
    margin: 10,
    marginTop: 40,
  },
  input: {
    marginBottom: 10,
  },
  textArea: {
    height: 150,
    width: '100%',
    padding: 0,
    margin: 0,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 20,
    marginBottom: 10,
    width: '95%',
  },
  buttonSend: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
});
