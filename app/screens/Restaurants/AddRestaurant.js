import { Button } from 'react-native-elements';
import { Formik } from 'formik';
import { getAuth } from 'firebase/auth';
import { map, size } from 'lodash';
import { useNavigation } from '@react-navigation/native';
import { useRef, useState } from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';
import uuid from 'random-uuid-v4';
import * as Yup from 'yup';
import colors from 'constants/colors';

import { addRestaurantService } from 'services/restaurant';
import { uploadImageService, getUrlImageService } from 'services/files/image.service';
import Gallery from 'components/Shared/Gallery';
import Input from 'components/Form/Input';
import Map from 'components/Map';
import Modal from 'components/Shared/Modal';
import Photo from 'components/Account/Photo';

export default function AddRestaurant() {
  const { navigate } = useNavigation();
  const [images, setImages] = useState([]);
  const [locationRestaurant, setLocationRestaurant] = useState(null);
  const [isVisibleMap, setIsVisibleMap] = useState(false);

  return (
    <ScrollView style={styles.scrollView}>
      <View style={styles.viewForm}>
        <Photo uri={images[0]} />
        <Formik
          initialValues={{
            name: '',
            address: '',
            description: '',
          }}
          validationSchema={Yup.object({
            name: Yup.string().required('El nombre del restaurante no puede estar vacío.'),
            address: Yup.string().required('La dirección del restaurante no puede estar vacío.'),
            description: Yup.string().required(
              'La descripción del restaurante no puede estar vacío.',
            ),
          })}
          onSubmit={async (values) => {
            if (size(images) === 0) {
              Toast.show({
                type: 'error',
                text1: 'El restaurante debe agregar al menos una imagen.',
              });
              return;
            }

            if (!locationRestaurant) {
              Toast.show({
                type: 'error',
                text1: 'Agregue una localización al restaurante dado click en el icono de mapa.',
              });
              return;
            }

            /** save images */
            const promiseImages = map(images, async (uri) => {
              const name = uuid();
              await uploadImageService(uri, 'restaurants', name);
              const url = await getUrlImageService('restaurants', name);
              return url;
            });
            const urls = await Promise.all(promiseImages);
            /** save images */

            const restaurant = {
              ...values,
              images: urls,
              location: locationRestaurant,
              createBy: getAuth().currentUser.uid,
            };

            const { error, message } = await addRestaurantService(restaurant);
            !error && setImages([]);
            !error && setLocationRestaurant(null);
            error && Toast.show({ type: 'error', text1: message });
            !error && navigate('restaurants');
          }}
        >
          {({ handleChange, handleSubmit, setFieldTouched, errors, isSubmitting, touched }) => (
            <>
              <Input
                style={styles.input}
                placeholder='Nombre'
                onChangeText={handleChange('name')}
                onBlur={() => setFieldTouched('name', true, true)}
                errorMessage={touched.name && errors.name ? errors.name : ''}
              />

              <Input
                style={styles.input}
                placeholder='Dirección'
                iconName='google-maps'
                iconOnPress={() => setIsVisibleMap(true)}
                iconStyle={locationRestaurant ? styles.iconRightSelected : styles.iconRight}
                onChangeText={handleChange('address')}
                onBlur={() => setFieldTouched('address', true, true)}
                errorMessage={touched.address && errors.address ? errors.address : ''}
              />

              <Input
                style={styles.textArea}
                multiline={true}
                placeholder='Descripción'
                onChangeText={handleChange('description')}
                onBlur={() => setFieldTouched('description', true, true)}
                errorMessage={touched.description && errors.description ? errors.description : ''}
              />

              <Gallery images={images} setImages={setImages} />

              <Button
                title='Guardar'
                onPress={handleSubmit}
                containerStyle={styles.containerButton}
                buttonStyle={styles.button}
                loading={isSubmitting}
                disabled={isSubmitting}
              />
            </>
          )}
        </Formik>
      </View>
      {isVisibleMap && (
        <Modal isVisible={isVisibleMap} setIsVisible={setIsVisibleMap}>
          <Map setIsVisibleMap={setIsVisibleMap} setLocation={setLocationRestaurant} />
        </Modal>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    height: '100%',
  },
  viewForm: {
    marginLeft: 10,
    marginRight: 10,
  },
  input: {
    marginBottom: 10,
  },
  iconRight: {
    color: '#c1c1c1',
  },
  iconRightSelected: {
    color: colors.PRIMARY_COLOR,
  },
  textArea: {
    height: 100,
    width: '100%',
    padding: 0,
    margin: 0,
  },
  containerButton: {
    marginTop: 20,
    width: '100%',
  },
  button: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
});
