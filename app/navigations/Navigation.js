import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import RestaurantsStack from 'navigations/RestaurantsStack';
import FavoritesStack from 'navigations/FavoritesStack';
import TopRestaurantsStack from 'navigations/TopRestaurantsStack';
import SearchStack from 'navigations/SearchStack';
import AccountStack from 'navigations/AccountStack';
import { Icon } from 'react-native-elements';
import colors from 'constants/colors';

const Tab = createBottomTabNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarInactiveTintColor: '#646464',
          tabBarActiveTintColor: colors.PRIMARY_COLOR,
          tabBarIcon: ({ color }) => <TabIcon route={route} color={color} />,
        })}
      >
        <Tab.Screen
          name='restaurants-stack'
          component={RestaurantsStack}
          options={{ title: 'Restaurantes', headerShown: false }}
        />
        <Tab.Screen
          name='favorites-stack'
          component={FavoritesStack}
          options={{ title: 'Favoritos', headerShown: false }}
        />
        <Tab.Screen
          name='top-restaurants-stack'
          component={TopRestaurantsStack}
          options={{ title: 'Top 5', headerShown: false }}
        />
        <Tab.Screen
          name='search-stack'
          component={SearchStack}
          options={{ title: 'Buscar', headerShown: false }}
        />
        <Tab.Screen
          name='account-stack'
          component={AccountStack}
          options={{ title: 'Perfil', headerShown: false }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

function TabIcon({ route, color }) {
  let iconName;

  switch (route.name) {
    case 'restaurants-stack':
      iconName = 'compass-outline';
      break;
    case 'favorites-stack':
      iconName = 'heart-outline';
      break;
    case 'top-restaurants-stack':
      iconName = 'star-outline';
      break;
    case 'search-stack':
      iconName = 'magnify';
      break;
    case 'account-stack':
      iconName = 'home-outline';
      break;
  }

  return <Icon type='material-community' name={iconName} size={22} color={color} />;
}
