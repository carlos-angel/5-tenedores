export default {
  PRIMARY_COLOR: '#00a680',
  SECONDARY_COLOR: '#4B5563',
  TERTIARY_COLOR: '#1E293B',
  BACKGROUND: '#ffffff',
};
