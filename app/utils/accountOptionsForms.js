import * as Yup from 'yup';
import {
  updateUserService,
  updateEmailService,
  updatePasswordService,
} from 'services/account/update-user.service';

export default [
  {
    form: 'displayName',
    isReauthenticate: false,
    inputs: [
      {
        type: 'input',
        placeholder: 'Nombre',
        name: 'displayName',
        value: '',
        iconName: 'account-circle-outline',
        onPressIcon: () => {},
        secureTextEntry: false,
        schema: Yup.string().required('El campo nombre no puede estar vació.'),
      },
    ],
    onSubmit: async (values) => {
      const { displayName } = values;
      try {
        await updateUserService({ displayName });
        return { error: false, message: 'nombre actualizado' };
      } catch (error) {
        return { error: true, message: 'Ops! Algo paso, inténtelo más tarde.' };
      }
    },
  },
  {
    form: 'email',
    isReauthenticate: true,
    inputs: [
      {
        type: 'input',
        placeholder: 'Email',
        name: 'email',
        value: '',
        iconName: 'at',
        onPressIcon: () => {},
        secureTextEntry: false,
        schema: Yup.string()
          .email('Ingrese un email valido.')
          .required('El campo email no puede estar vació.'),
      },
      {
        type: 'input',
        placeholder: 'contraseña',
        name: 'password',
        value: '',
        iconName: 'eye-outline',
        secureTextEntry: true,
        onPressIcon: () => {},
        schema: Yup.string()
          .min(8, 'La contraseña debe tener mínimo 8 caracteres')
          .required('El campo contraseña no puede estar vació.'),
      },
    ],
    onSubmit: async (values) => {
      const { email } = values;
      try {
        await updateEmailService(email);
        return { error: false, message: 'email actualizado' };
      } catch (error) {
        return { error: true, message: 'Ops! Algo paso, inténtelo más tarde.' };
      }
    },
  },
  {
    form: 'password',
    isReauthenticate: true,
    inputs: [
      {
        type: 'input',
        placeholder: 'contraseña actual',
        name: 'password',
        value: '',
        iconName: 'eye-outline',
        secureTextEntry: true,
        onPressIcon: () => {},
        schema: Yup.string()
          .min(8, 'La contraseña debe tener mínimo 8 caracteres')
          .required('El campo contraseña no puede estar vació.'),
      },
      {
        type: 'input',
        placeholder: 'nueva contraseña',
        name: 'newPassword',
        value: '',
        iconName: 'eye-outline',
        secureTextEntry: true,
        onPressIcon: () => {},
        schema: Yup.string()
          .min(8, 'La contraseña debe tener mínimo 8 caracteres')
          .required('El campo contraseña no puede estar vació.'),
      },
      {
        type: 'input',
        placeholder: 'Confirmar nueva contraseña',
        name: 'confirmPassword',
        value: '',
        iconName: 'eye-outline',
        secureTextEntry: true,
        onPressIcon: () => {},
        schema: Yup.string()
          .min(8, 'La contraseña debe tener mínimo 8 caracteres')
          .oneOf([Yup.ref('newPassword')], 'Las contraseñas no coinciden.')
          .required('Favor de confirmar su nueva contraseña.'),
      },
    ],
    onSubmit: async (values) => {
      const { newPassword } = values;
      try {
        await updatePasswordService(newPassword);
        return { error: false, message: 'contraseña actualizada' };
      } catch (error) {
        return { error: true, message: 'Ops! Algo paso, inténtelo más tarde.' };
      }
    },
  },
];
