export const getValidationSchema = (inputs) => {
  let validationSchema={};
  inputs.forEach(input => {
    const { schema, name } = input;
    if(!schema) return;
    validationSchema[name] = schema;
  });

  return validationSchema;
}