export const getColorByPositionTopRestaurant = (positionTopRestaurant) => {
  let color;
  switch (positionTopRestaurant) {
    case 1:
      color = '#efb819';
      break;
    case 2:
      color = '#e3e4e5';
      break;
    case 3:
      color = '#cd7f32';
      break;
    default:
      color = '#000000';
      break;
  }
  return color;
};
