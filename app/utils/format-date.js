export const formatDate = (date) => {
  const dateSeconds = new Date(date.seconds * 1000);
  const days = dateSeconds.getDate();
  const month = dateSeconds.getMonth() + 1;
  const years = dateSeconds.getFullYear();
  const hours = dateSeconds.getHours();
  const minutes = `${dateSeconds.getMinutes() < 10 ? '0' : ''}${dateSeconds.getMinutes()}`;

  return `${days}/${month}/${years} ${hours}:${minutes}`;
};
