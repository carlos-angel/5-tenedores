export const getInitialValues = (inputs, values = {}) => {
  let initialValues={};
  inputs.forEach(input => {
    const { name, value } = input;

    initialValues[name] = values[name] ?? value;
  });

  return initialValues;
}