export default [
  {
    title: 'Cambiar Nombre',
    iconType: 'material-community',
    iconNameLeft: 'account-circle',
    iconColorLeft: '#ccc',
    iconNameRight: 'chevron-right',
    iconColorRight: '#ccc',
    key: 'displayName',
  },
  {
    title: 'Cambiar Email',
    iconType: 'material-community',
    iconNameLeft: 'at',
    iconColorLeft: '#ccc',
    iconNameRight: 'chevron-right',
    iconColorRight: '#ccc',
    key: 'email',
  },
  {
    title: 'Cambiar Contraseña',
    iconType: 'material-community',
    iconNameLeft: 'lock-reset',
    iconColorLeft: '#ccc',
    iconNameRight: 'chevron-right',
    iconColorRight: '#ccc',
    key: 'password',
  },
];
