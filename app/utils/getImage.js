import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';

export const getImage = async () => {
  const { status } = await Camera.requestCameraPermissionsAsync();
  if (status === 'denied') {
    return { error: true, uri: false, message: 'Es necesario aceptar los permisos de la galería' };
  } else {
    const image = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    if (image.cancelled) {
      return {
        error: true,
        uri: false,
        message: 'Ha cerrado la galería sin seleccionar una imagen',
      };
    }

    return { error: false, uri: image.uri, message: 'imagen seleccionada' };
  }
};
