import { useState } from 'react';

export const useChangeNameIcon = (nameCurrentIcon, nameOldIcon) => {
  const [nameIcon, setNameIcon] = useState(nameCurrentIcon);

  const changeNameIcon = () => {
    nameIcon === nameCurrentIcon ? setNameIcon(nameOldIcon) : setNameIcon(nameCurrentIcon);
  }

  return [nameIcon, changeNameIcon];
};
