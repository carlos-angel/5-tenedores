import { useContext } from 'react';
import { AccountContext } from 'context/account.context';

export const useAccount = () => useContext(AccountContext);
