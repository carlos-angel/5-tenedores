import { useState, useEffect } from 'react';
import { onAuthStateChanged, getAuth } from 'firebase/auth';

export const useAuthenticated = () => {
  const [auth, setAuth] = useState({ login: false, loading: true });
  const { login, loading } = auth;

  useEffect(() => {
    const auth = getAuth();
    onAuthStateChanged(auth, (user) => {
      !user ? setAuth({ login: false, loading: false }) : setAuth({ login: true, loading: false });
    });
  }, []);

  return { isAuth: login, loading };
};
