import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage';

export const uploadImageService = async (uri, folder, imageName) => {
  try {
    const response = await fetch(uri);
    const blob = await response.blob();

    const storage = getStorage();
    const storageRef = ref(storage, `${folder}/${imageName}`);

    await uploadBytes(storageRef, blob);
    return true;
  } catch (error) {
    return false;
  }
};

export const getUrlImageService = async (folder, imageName) => {
  try {
    const storage = getStorage();
    const storageRef = ref(storage, `${folder}/${imageName}`);
    const url = await getDownloadURL(storageRef);
    return url;
  } catch (error) {
    return '';
  }
};
