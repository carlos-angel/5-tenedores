import { getAuth } from 'firebase/auth';
import { codeErrorsFirebase } from 'utils/codeErrorMessagesFirebase';

export const informationUserService = () => {
  const auth = getAuth();
  try {
    const data = auth.currentUser;
    return { error: false, data };
  } catch (error) {
    const code = error['code'];
    const message = codeErrorsFirebase[code] || 'Ops. Algo salio mal, inténtelo más tarde.';
    return { error: true, message };
  }
};
