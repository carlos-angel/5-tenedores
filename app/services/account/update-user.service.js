import { getAuth, updateProfile, updateEmail, updatePassword } from 'firebase/auth';

export const updateUserService = async (update) => {
  try {
    const auth = getAuth();
    await updateProfile(auth.currentUser, update);
    return true;
  } catch (error) {
    return false;
  }
};

export const updateEmailService = async (email) => {
  try {
    const auth = getAuth();
    await updateEmail(auth.currentUser, email);
    return true;
  } catch (error) {
    return false;
  }
};

export const updatePasswordService = async (password) => {
  try {
    const auth = getAuth();
    await updatePassword(auth.currentUser, password);
    return true;
  } catch (error) {
    return false;
  }
};
