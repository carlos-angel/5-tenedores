import { getStorage, getDownloadURL, ref } from 'firebase/storage';

export const getAvatarService = async (avatarName) => {
  try {
    const storage = getStorage();
    const refStorage = ref(storage, `avatar/${avatarName}`);

    return await getDownloadURL(refStorage);
  } catch (error) {
    return false;
  }
};
