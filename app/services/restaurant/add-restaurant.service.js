import { getFirestore, addDoc, collection } from 'firebase/firestore';

export const addRestaurantService = async (data) => {
  try {
    const db = getFirestore();
    const restaurant = {
      ...data,
      rating: 0,
      ratingTotal: 0,
      quantityVoting: 0,
      createAt: new Date(),
    };

    await addDoc(collection(db, 'restaurants'), restaurant);
    return { error: false, message: 'restaurante registrado correctamente.' };
  } catch (error) {
    return { error: true, message: 'Error al subir el restaurante, inténtenlo más tarde' };
  }
};
