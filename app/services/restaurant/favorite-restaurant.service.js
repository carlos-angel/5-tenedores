import { getAuth } from 'firebase/auth';
import {
  getFirestore,
  addDoc,
  collection,
  deleteDoc,
  query,
  limit,
  doc,
  where,
  getDocs,
} from 'firebase/firestore';
import { getRestaurantsById } from 'services/restaurant/get-restaurant.service';

export const changeFavorite = async ({ isFavorite, idRestaurant }) => {
  try {
    if (isFavorite) {
      await addRestaurantFavoriteService(idRestaurant);
    } else {
      const { error, id } = await getRestaurantFavorite(idRestaurant);
      if (!error) {
        await removeRestaurantFavoriteService(id);
      }
    }
    return { error: false };
  } catch (error) {
    return { error: true };
  }
};

export const getRestaurantFavorite = async (idRestaurant) => {
  try {
    const db = getFirestore();
    const idUser = getAuth().currentUser.uid;

    const favoritesRef = collection(db, 'favorites');
    const queryRef = query(
      favoritesRef,
      where('idUser', '==', idUser),
      where('idRestaurant', '==', idRestaurant),
      limit(1),
    );
    const favoriteSnapshot = await getDocs(queryRef);

    const favorite = favoriteSnapshot.docs[0];
    return { error: false, id: favorite.id };
  } catch (error) {
    return { error: true };
  }
};

export const addRestaurantFavoriteService = async (idRestaurant) => {
  try {
    const db = getFirestore();
    const idUser = getAuth().currentUser.uid;
    const payload = {
      idUser,
      idRestaurant,
    };
    await addDoc(collection(db, 'favorites'), payload);
    return { error: false };
  } catch (error) {
    return { error: true };
  }
};

export const removeRestaurantFavoriteService = async (id) => {
  try {
    const db = getFirestore();

    const refDoc = doc(db, `favorites/${id}`);
    await deleteDoc(refDoc);
    return { error: false };
  } catch (error) {
    return { error: true };
  }
};

export const getFavoritesRestaurants = async () => {
  try {
    const db = getFirestore();
    const idUser = getAuth().currentUser.uid;

    const favoritesRef = collection(db, 'favorites');
    const queryRef = query(favoritesRef, where('idUser', '==', idUser));
    const favoritesSnapshot = await getDocs(queryRef);

    const IdRestaurants = favoritesSnapshot.docs.map((favorite) => favorite.data().idRestaurant);

    const restaurantsPromise = IdRestaurants.map(async (id) => await getRestaurantsById(id));

    const restaurants = await Promise.all(restaurantsPromise);

    return restaurants || [];
  } catch (error) {
    return [];
  }
};
