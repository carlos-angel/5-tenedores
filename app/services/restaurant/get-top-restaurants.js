import { collection, getDocs, getFirestore, limit, orderBy, query } from 'firebase/firestore';

export const getTopRestaurants = async (limitRestaurants = 10) => {
  try {
    const db = getFirestore();
    const restaurantRef = collection(db, 'restaurants');

    const queryRef = query(restaurantRef, orderBy('rating', 'desc'), limit(limitRestaurants));

    const restaurantsSnapshot = await getDocs(queryRef);

    const restaurants = restaurantsSnapshot.docs.map((restaurant, index) => ({
      id: restaurant.id,
      ...restaurant.data(),
      positionTop: index + 1,
    }));

    return restaurants || [];
  } catch (error) {
    return [];
  }
};
