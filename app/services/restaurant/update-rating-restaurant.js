import { getDoc, getFirestore, doc, updateDoc } from 'firebase/firestore';
import { getRestaurantsById } from './get-restaurant.service';

export const updateRatingRestaurant = async (idRestaurant, rating) => {
  try {
    const db = getFirestore();
    const docRef = doc(db, 'restaurants', idRestaurant);
    const payload = await calculateRatingRestaurant(idRestaurant, rating);
    await updateDoc(docRef, payload);
  } catch (error) {
    return error;
  }
};

const calculateRatingRestaurant = async (idRestaurant, rating) => {
  try {
    const restaurant = await getRestaurantsById(idRestaurant);
    const ratingTotal = restaurant.ratingTotal + rating;
    const quantityVoting = restaurant.quantityVoting + 1;

    const ratingResult = ratingTotal / quantityVoting;
    const payload = { rating: ratingResult, quantityVoting, ratingTotal };
    return payload;
  } catch (error) {
    return error;
  }
};
