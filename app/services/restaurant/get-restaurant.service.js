import { collection, getDoc, getFirestore, doc } from 'firebase/firestore';

export const getRestaurantsById = async (id) => {
  try {
    const db = getFirestore();
    const docRef = doc(db, 'restaurants', id);

    const restaurantSnapshot = await getDoc(docRef);
    const existRestaurant = restaurantSnapshot.exists();

    const restaurant = existRestaurant
      ? { id: restaurantSnapshot.id, ...restaurantSnapshot.data() }
      : false;

    return restaurant;
  } catch (error) {
    return error;
  }
};
