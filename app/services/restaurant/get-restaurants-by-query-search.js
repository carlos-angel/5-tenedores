import {
  collection,
  getDocs,
  getFirestore,
  limit,
  orderBy,
  query,
  startAt,
  endAt,
} from 'firebase/firestore';

export const getRestaurantsByQuerySearch = async (searchQuery, limitResult = 10) => {
  try {
    const db = getFirestore();
    const restaurantRef = collection(db, 'restaurants');
    const queryRef = query(
      restaurantRef,
      orderBy('name'),
      startAt(searchQuery),
      endAt(`${searchQuery}\uf8ff`),
      limit(limitResult),
    );

    const restaurantsSnapshot = await getDocs(queryRef);

    const restaurants = restaurantsSnapshot.docs.map((restaurant) => ({
      id: restaurant.id,
      ...restaurant.data(),
    }));

    return restaurants || [];
  } catch (error) {
    return [];
  }
};
