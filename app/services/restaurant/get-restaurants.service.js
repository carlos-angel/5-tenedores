import {
  collection,
  getDocs,
  getFirestore,
  limit,
  orderBy,
  query,
  startAfter,
} from 'firebase/firestore';

export const getRestaurantsByLimit = async ({ startAfterCreateAt, limitRestaurants = 10 }) => {
  try {
    const db = getFirestore();
    const restaurantRef = collection(db, 'restaurants');

    const queryRef = startAfterCreateAt
      ? query(
          restaurantRef,
          orderBy('createAt', 'desc'),
          startAfter(startAfterCreateAt),
          limit(limitRestaurants),
        )
      : query(restaurantRef, orderBy('createAt', 'desc'), limit(limitRestaurants));

    const restaurantsSnapshot = await getDocs(queryRef);

    const restaurants = restaurantsSnapshot.docs.map((restaurant) => ({
      id: restaurant.id,
      ...restaurant.data(),
    }));

    return restaurants || [];
  } catch (error) {
    return [];
  }
};
