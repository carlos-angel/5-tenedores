import {getAuth, signOut} from 'firebase/auth';

export const signOutService = async () => {
  try {
    const auth = getAuth();
    await signOut(auth);
  } catch (error) {
    throw new Error('Ops. Algo salio mal, inténtelo más tarde.');
  }
}
