import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import { codeErrorsFirebase } from 'utils/codeErrorMessagesFirebase';

export const signInService = async ({ email, password }) => {
  try {
    const auth = getAuth();
    await signInWithEmailAndPassword(auth, email, password);
    return { error: false, message: '' };
  } catch (error) {
    const code = error['code'];
    const message = codeErrorsFirebase[code] || 'Ops. Algo salio mal, inténtelo más tarde.';
    return { error: true, message };
  }
};
