import { getAuth, reauthenticateWithCredential, EmailAuthProvider } from 'firebase/auth';

export const reauthenticateService = async (password) => {
  try {
    const auth = getAuth();
    const { email } = auth.currentUser;
    const credentials = EmailAuthProvider.credential(email, password);
    await reauthenticateWithCredential(auth.currentUser, credentials);
    return true;
  } catch (error) {
    return false;
  }
};
