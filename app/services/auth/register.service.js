import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import { codeErrorsFirebase } from 'utils/codeErrorMessagesFirebase';

export const registerService = async ({ email, password }) => {
  const auth = getAuth();
  try {
    await createUserWithEmailAndPassword(auth, email, password);
    return { error: false, message: '' };
  } catch (error) {
    const code = error['code'];
    const message = codeErrorsFirebase[code] || 'Ops. Algo salio mal, inténtelo más tarde.';
    return { error: true, message };
  }
};
