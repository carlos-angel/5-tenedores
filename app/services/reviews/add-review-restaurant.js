import { getFirestore, addDoc, collection } from 'firebase/firestore';

export const addReviewRestaurantService = async (payload) => {
  try {
    const db = getFirestore();
    const review = {
      ...payload,
      createAt: new Date(),
    };

    await addDoc(collection(db, 'reviews'), review);
    return { error: false, message: 'comentario guardado' };
  } catch (error) {
    return { error: true, message: 'Error al subir el comentario, inténtenlo más tarde' };
  }
};
