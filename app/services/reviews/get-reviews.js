import {
  collection,
  getDocs,
  getFirestore,
  limit,
  orderBy,
  query,
  where,
  startAfter,
} from 'firebase/firestore';

export const getReviewByIdRestaurantAndLimit = async ({
  idRestaurant,
  startAfterCreateAt,
  limitReviews = 10,
}) => {
  try {
    const db = getFirestore();
    const reviewsRef = collection(db, 'reviews');

    const queryRef = startAfterCreateAt
      ? query(
          reviewsRef,
          where('idRestaurant', '==', idRestaurant),
          orderBy('createAt', 'desc'),
          startAfter(startAfterCreateAt),
          limit(limitReviews),
        )
      : query(
          reviewsRef,
          where('idRestaurant', '==', idRestaurant),
          orderBy('createAt', 'desc'),
          limit(limitReviews),
        );

    const reviewsSnapshot = await getDocs(queryRef);

    const reviews = reviewsSnapshot.docs.map((review) => ({
      id: review.id,
      ...review.data(),
    }));
    return reviews || [];
  } catch (error) {
    return [];
  }
};
