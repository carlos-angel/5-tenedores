import { useChangeNameIcon } from 'hooks/useChangeNameIcon';
import { Input as InputNativeElement, Icon } from 'react-native-elements';

export default function Input(props) {
  const { iconName, iconStyle, iconOnPress, secureTextEntry, ...inputProps } = props;
  let isSecureTextEntry = secureTextEntry;

  /**Input with secureTextEntry in true */
  if (secureTextEntry) {
    const [iconName, onPressIcon] = useChangeNameIcon('eye-outline', 'eye-off-outline');
    isSecureTextEntry = iconName === 'eye-outline';

    return (
      <InputNativeElement
        {...inputProps}
        secureTextEntry={isSecureTextEntry}
        rightIcon={
          <Icon
            type='material-community'
            name={iconName}
            iconStyle={iconStyle}
            onPress={onPressIcon}
          />
        }
      />
    );
  }

  /**Input with icon */
  if (iconName) {
    return (
      <InputNativeElement
        {...inputProps}
        secureTextEntry={isSecureTextEntry}
        rightIcon={
          <Icon
            type='material-community'
            name={iconName}
            iconStyle={iconStyle}
            onPress={iconOnPress}
          />
        }
      />
    );
  }

  /**Input simple */
  return <InputNativeElement {...inputProps} secureTextEntry={isSecureTextEntry} />;
}
