import { View, Text, StyleSheet, ActivityIndicator, TouchableOpacity, Alert } from 'react-native';
import { Icon, Image } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { changeFavorite } from 'services/restaurant/favorite-restaurant.service';
import colors from 'constants/colors';

export default function FavoriteRestaurantItem(props) {
  const { id, images, name, setIsLoading, navigation } = props;
  const uri = images[0] ?? '';

  const removeFavorite = () => {
    setIsLoading(true);
    changeFavorite({ isFavorite: false, idRestaurant: id })
      .then()
      .catch(() =>
        Toast.show({ type: 'error', text1: 'Error al eliminar favorito, Inténtelo más tarde' }),
      )
      .finally(() => setIsLoading(false));
  };

  const confirmRemoveFavorite = () => {
    Alert.alert(
      'Eliminar restaurante de favoritos',
      '¿Estás seguro que quieres eliminar este restaurante de favoritos?',
      [
        {
          text: 'Canelar',
          style: 'cancel',
        },
        {
          text: 'Eliminar',
          onPress: removeFavorite,
        },
      ],
      {
        cancelable: false,
      },
    );
  };

  return (
    <View style={styles.restaurant}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('restaurants-stack', { screen: 'restaurant', params: { id, name } })
        }
      >
        <Image
          resizeMode='cover'
          style={styles.image}
          PlaceholderContent={<ActivityIndicator color={colors.BACKGROUND} />}
          source={uri ? { uri } : require('assets/no-image.png')}
        />
        <View style={styles.info}>
          <Text style={styles.nameRestaurant}>{name}</Text>
          <Icon
            type='material-community'
            name='heart'
            color='#f00'
            containerStyle={styles.favorite}
            onPress={confirmRemoveFavorite}
            underlayColor='transparent'
            size={30}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  restaurant: {
    margin: 10,
  },
  image: {
    width: '100%',
    height: 180,
  },
  info: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: -30,
    backgroundColor: colors.BACKGROUND,
  },
  nameRestaurant: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.TERTIARY_COLOR,
  },
  favorite: {
    marginTop: -35,
    backgroundColor: colors.BACKGROUND,
    padding: 20,
    borderRadius: 100,
    zIndex: 2,
  },
});
