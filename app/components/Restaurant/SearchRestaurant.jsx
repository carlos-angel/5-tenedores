import { Text, ActivityIndicator } from 'react-native';
import { ListItem, Icon, Avatar } from 'react-native-elements';
import colors from 'constants/colors';

export default function SearchRestaurant(props) {
  const { id, name, images, navigation } = props;
  const uri = images[0];
  return (
    <ListItem
      onPress={() =>
        navigation.navigate('restaurants-stack', { screen: 'restaurant', params: { id, name } })
      }
    >
      <Avatar
        rounded
        placeholder={<ActivityIndicator color={colors.BACKGROUND} />}
        source={uri ? { uri } : require('assets/no-image.png')}
      />
      <ListItem.Content>
        <Text style={{ color: colors.TERTIARY_COLOR }}>{name}</Text>
      </ListItem.Content>
      <Icon type='material-community' name='chevron-right' color={colors.TERTIARY_COLOR} />
    </ListItem>
  );
}
