import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import colors from 'constants/colors';

export default function Information({ children }) {
  return <View style={styles.viewInformationRestaurant}>{children}</View>;
}

function Title({ children }) {
  return <View style={styles.viewRestaurantName}>{children}</View>;
}

function Description({ description }) {
  return <Text style={styles.descriptionRestaurant}>{description}</Text>;
}

Information.Title = Title;
Information.Description = Description;

const styles = StyleSheet.create({
  viewInformationRestaurant: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 15,
  },
  viewRestaurantName: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  descriptionRestaurant: {
    marginTop: 8,
    textAlign: 'center',
    color: colors.SECONDARY_COLOR,
  },
});
