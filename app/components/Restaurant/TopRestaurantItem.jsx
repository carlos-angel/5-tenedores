import { Card, Icon, Image, Rating } from 'react-native-elements';
import { View, Text, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native';
import { getColorByPositionTopRestaurant } from 'utils/colorTopRestaurant';
import colors from 'constants/colors';

export default function TopRestaurantItem(props) {
  const { id, name, positionTop, images, rating, description, navigation } = props;
  const color = getColorByPositionTopRestaurant(positionTop);
  const uri = images[0];

  return (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('restaurants-stack', { screen: 'restaurant', params: { id, name } })
      }
    >
      <Card containerStyle={StyleSheet.containerCard}>
        <Icon
          type='material-community'
          name='chess-queen'
          size={40}
          color={color}
          containerStyle={styles.containerIcon}
        />
        <Image
          style={styles.imageRestaurant}
          resizeMode='cover'
          source={uri ? { uri } : require('assets/no-image.png')}
          PlaceholderContent={<ActivityIndicator color={colors.BACKGROUND} />}
        />
        <View style={styles.titleRating}>
          <Text style={styles.title}>{name}</Text>
          <Rating imageSize={20} startingValue={rating} readonly />
        </View>
        <Text style={styles.description}>{description}</Text>
      </Card>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  containerCard: {
    marginBottom: 30,
    borderWidth: 0,
  },
  containerIcon: {
    position: 'absolute',
    top: -30,
    left: -30,
    zIndex: 1,
  },
  imageRestaurant: {
    width: '100%',
    height: 200,
  },
  titleRating: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
    color: colors.TERTIARY_COLOR,
  },
  description: {
    color: colors.SECONDARY_COLOR,
    paddingTop: 0,
    textAlign: 'justify',
  },
});
