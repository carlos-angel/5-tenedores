import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import colors from 'constants/colors';

export default function Location({ title, children }) {
  return (
    <View style={styles.content}>
      <Text style={styles.title}>{title}</Text>
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    margin: 15,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    color: colors.TERTIARY_COLOR,
  },
});
