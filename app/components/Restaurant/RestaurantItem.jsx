import { View, Text, StyleSheet, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Image, Rating } from 'react-native-elements';
import colors from 'constants/colors';

export default function RestaurantItem(props) {
  const { id, name, images, address, rating, navigation } = props;
  const uri = images ? images[0] : '';
  const goRestaurant = () => navigation.navigate('restaurant', { id, name });

  return (
    <TouchableOpacity onPress={() => goRestaurant()}>
      <View style={styles.viewRestaurant}>
        <View style={styles.ViewImageRestaurant}>
          <Image
            resizeMethod='auto'
            PlaceholderContent={<ActivityIndicator color={colors.BACKGROUND} />}
            source={uri ? { uri } : require('assets/no-image.png')}
            style={styles.imageRestaurant}
          />
        </View>
        <View style={styles.viewInformation}>
          <Text style={styles.restaurantName}>{name}</Text>
          <Text style={styles.restaurantAddress}>{address}</Text>
          <Rating imageSize={20} readonly startingValue={rating} />
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  viewRestaurant: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    margin: 10,
  },
  viewInformation: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  ViewImageRestaurant: {
    paddingRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageRestaurant: {
    width: 85,
    height: 85,
  },
  restaurantName: {
    fontWeight: 'bold',
    color: colors.TERTIARY_COLOR,
  },
  restaurantAddress: {
    color: colors.SECONDARY_COLOR,
    fontSize: 11,
    width: 280,
  },
});
