import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { useState, useEffect } from 'react';
import Toast from 'react-native-toast-message';
import * as Location from 'expo-location';
import MapView, { Marker } from 'react-native-maps';
import { Button } from 'react-native-elements';
import colors from 'constants/colors';

export default function Map({ setLocation, setIsVisibleMap }) {
  const [currentLocation, setCurrentLocation] = useState(null);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        Toast.show({
          type: 'error',
          text1: 'Habilitar permisos de localización (gps)',
          text2: 'Se requiere habilitar los permisos para crear un restaurante',
        });
        return;
      }

      let { coords } = await Location.getCurrentPositionAsync({});
      const { latitude, longitude } = coords;
      setCurrentLocation({ latitude, longitude, latitudeDelta: 0.001, longitudeDelta: 0.001 });
    })();
  }, []);

  const onConfirm = () => {
    setLocation(currentLocation);
    Toast.show({ type: 'success', text1: 'Localización guardada correctamente' });
    setIsVisibleMap(false);
  };

  const onCancel = () => setIsVisibleMap(false);

  return (
    <View>
      <View>
        {currentLocation ? (
          <MapView
            style={styles.map}
            initialRegion={currentLocation}
            showsUserLocation={true}
            onRegionChange={(region) => {
              setCurrentLocation(region);
            }}
          >
            <Marker
              coordinate={{
                latitude: currentLocation.latitude,
                longitude: currentLocation.longitude,
              }}
            />
          </MapView>
        ) : (
          <View>
            <ActivityIndicator size='large' color={colors.PRIMARY_COLOR} />
            <Text style={styles.textLoading}>Cargando Mapa</Text>
          </View>
        )}
      </View>
      <View style={styles.viewMapButtons}>
        <Button
          title='Guardar ubicación'
          containerStyle={styles.buttonContainerSetMap}
          buttonStyle={styles.buttonSetMap}
          onPress={onConfirm}
        />
        <Button
          title='cancelar'
          containerStyle={styles.buttonContainerCancelMap}
          buttonStyle={styles.buttonCancelMap}
          onPress={onCancel}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  map: {
    width: '100%',
    height: 550,
  },
  viewMapButtons: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
  },
  buttonContainerCancelMap: {
    paddingLeft: 5,
  },
  buttonCancelMap: {
    backgroundColor: '#a60d0d',
  },
  buttonContainerSetMap: {
    paddingRight: 5,
  },
  buttonSetMap: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
  textLoading: {
    color: colors.PRIMARY_COLOR,
    textTransform: 'uppercase',
    textAlign: 'center',
    marginTop: 10,
  },
});
