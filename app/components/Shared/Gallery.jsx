import { getImage } from 'utils/getImage';
import { View, StyleSheet, Alert } from 'react-native';
import Toast from 'react-native-toast-message';
import { Avatar, Icon } from 'react-native-elements';
import { map, size, filter } from 'lodash';

export default function Gallery({ images, setImages, limit = 4 }) {
  const onPress = async () => {
    const { error, message, uri } = await getImage();
    error && Toast.show({ type: 'error', text1: message });
    if (!error && uri) {
      setImages((data) => [...data, uri]);
    }
  };

  const removeImage = (image) => {
    const imagesTemp = images;
    Alert.alert(
      'Eliminar imagen',
      '¿Estás seguro que quieres eliminar esta imagen?',
      [
        {
          text: 'cancel',
          style: 'cancel',
        },
        {
          text: 'eliminar',
          onPress: () => {
            const imagesFilter = filter(imagesTemp, (imageUri) => imageUri !== image);
            setImages(imagesFilter);
          },
        },
      ],
      { cancelable: false },
    );
  };

  return (
    <View style={styles.viewImage}>
      {size(images) < limit && <UploadImage onPress={onPress} />}
      {map(images, (uri, index) => (
        <Avatar
          key={index}
          source={{ uri }}
          containerStyle={styles.miniatureStyle}
          onPress={() => removeImage(uri)}
        />
      ))}
    </View>
  );
}

function UploadImage({ onPress }) {
  return (
    <Icon
      type='material-community'
      name='camera'
      color='#7a7a7a'
      containerStyle={styles.containerImage}
      onPress={onPress}
    />
  );
}

const styles = StyleSheet.create({
  viewImage: {
    flexDirection: 'row',
    marginLeft: 20,
    marginRight: 20,
  },
  containerImage: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    height: 70,
    width: 70,
    backgroundColor: '#e3e3e3',
  },
  miniatureStyle: {
    width: 70,
    height: 70,
    marginRight: 10,
  },
});
