import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import React from 'react';
import { Overlay } from 'react-native-elements';
import colors from 'constants/colors';

export default function Loading({ isVisible, text }) {
  return (
    <Overlay
      isVisible={isVisible}
      backdropStyle={styles.overlayBackdrop}
      overlayStyle={styles.overlay}
    >
      <View style={styles.view}>
        <ActivityIndicator size='large' color={colors.PRIMARY_COLOR} />
        {text && <Text style={styles.text}>{text}</Text>}
      </View>
    </Overlay>
  );
}

const styles = StyleSheet.create({
  overlay: {
    height: 100,
    width: 200,
    backgroundColor: colors.BACKGROUND,
    borderColor: colors.PRIMARY_COLOR,
    borderWidth: 2,
    borderRadius: 10,
  },
  overlayBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: colors.PRIMARY_COLOR,
    textTransform: 'uppercase',
    marginTop: 10,
  },
});
