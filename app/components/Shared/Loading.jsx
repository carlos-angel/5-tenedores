import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import React from 'react';
import colors from 'constants/colors';

export default function Loading({ message }) {
  return (
    <View style={styles.loading}>
      <ActivityIndicator size='large' color={colors.PRIMARY_COLOR} />
      <Text style={styles.textLoading}>{message}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textLoading: {
    color: colors.PRIMARY_COLOR,
    textTransform: 'uppercase',
    marginTop: 10,
  },
});
