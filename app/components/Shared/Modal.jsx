import { StyleSheet } from 'react-native';
import React from 'react';
import { Overlay } from 'react-native-elements';
import colors from 'constants/colors';

export default function Modal({ isVisible, setIsVisible, children }) {
  const onClose = () => setIsVisible(false);

  return (
    <Overlay
      isVisible={isVisible}
      backdropStyle={styles.overlayBackdrop}
      overlayStyle={styles.overlay}
      onBackdropPress={onClose}
    >
      {children}
    </Overlay>
  );
}

const styles = StyleSheet.create({
  overlay: {
    height: 'auto',
    width: '90%',
    backgroundColor: colors.BACKGROUND,
  },
  overlayBackdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});
