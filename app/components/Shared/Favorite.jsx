import { StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { useState, useEffect } from 'react';
import {
  getRestaurantFavorite,
  changeFavorite,
} from 'services/restaurant/favorite-restaurant.service';
import colors from 'constants/colors';

export default function Favorite({ style, idRestaurant, sizeIcon }) {
  const [isFavorite, setIsFavorite] = useState(false);

  useEffect(() => {
    getRestaurantFavorite(idRestaurant)
      .then(({ id }) => setIsFavorite(!!id))
      .catch(() => setIsFavorite(false));
  }, [idRestaurant]);

  const onPress = () => {
    changeFavorite({ isFavorite: !isFavorite, idRestaurant });
    setIsFavorite((favorite) => !favorite);
  };

  return (
    <View style={style ?? styles.viewFavorite}>
      <Icon
        type='material-community'
        name={isFavorite ? 'heart' : 'heart-outline'}
        color={isFavorite ? '#f00' : '#000'}
        size={sizeIcon}
        underlayColor='transparent'
        onPress={onPress}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  viewFavorite: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 2,
    backgroundColor: colors.BACKGROUND,
    borderBottomLeftRadius: 30,
    padding: 5,
    paddingLeft: 15,
  },
});
