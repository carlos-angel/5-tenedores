import { Image } from 'react-native-elements';
import { useState } from 'react';
import { ActivityIndicator } from 'react-native';
import SnapCarousel, { Pagination } from 'react-native-snap-carousel';
import { size } from 'lodash';
import colors from 'constants/colors';

export default function Carousel({ images, height, width }) {
  const [activeSlice, setActiveSlice] = useState(0);
  const sizeImages = size(images);

  return (
    <>
      <SnapCarousel
        layout='default'
        data={images}
        sliderWidth={width}
        itemWidth={width}
        renderItem={({ item }) => (
          <Image
            PlaceholderContent={<ActivityIndicator color={colors.BACKGROUND} />}
            style={{ width, height }}
            source={{ uri: item }}
          />
        )}
        onSnapToItem={(index) => setActiveSlice(index)}
      />
      <Pagination
        dotsLength={sizeImages}
        activeDotIndex={activeSlice}
        containerStyle={{ backgroundColor: colors.BACKGROUND }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 8,
          backgroundColor: 'gray',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    </>
  );
}
