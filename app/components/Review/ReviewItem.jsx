import { View, Text, StyleSheet } from 'react-native';
import { Avatar, Rating } from 'react-native-elements';
import { formatDate } from 'utils/format-date';
import colors from 'constants/colors';

export default function ReviewItem(props) {
  const { avatar, title, review, rating, createAt } = props;
  const createReview = formatDate(createAt);
  return (
    <View style={styles.viewReview}>
      <View style={styles.viewImageAvatar}>
        <Avatar
          size='large'
          rounded
          containerStyle={styles.imageAvatar}
          source={avatar ? { uri: avatar } : require('assets/img/avatar-default.jpg')}
        />
      </View>
      <View style={styles.informationComment}>
        <Text style={styles.reviewTitle}>{title}</Text>
        <Text style={styles.reviewText}>{review}</Text>
        <Rating imageSize={15} readonly startingValue={rating} />
        <Text style={styles.reviewDate}>{createReview}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewReview: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderBottomColor: '#e3e3e3',
    borderBottomWidth: 1,
    paddingBottom: 5,
    paddingTop: 5,
  },
  viewImageAvatar: {
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageAvatar: {
    width: 55,
    height: 55,
  },
  informationComment: {
    flex: 1,
    alignItems: 'flex-start',
  },
  reviewTitle: {
    fontWeight: 'bold',
    color: colors.TERTIARY_COLOR,
  },
  reviewText: {
    paddingTop: 2,
    color: colors.SECONDARY_COLOR,
    marginBottom: 5,
  },
  reviewDate: {
    marginTop: 5,
    color: 'grey',
    fontSize: 12,
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
});
