import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { useAuthenticated } from 'hooks/useAuthenticated';
import colors from 'constants/colors';

export default function ButtonReview({ navigation, idRestaurant }) {
  const { isAuth } = useAuthenticated();

  return (
    <View>
      {isAuth ? (
        <Button
          title='Escribir comentario'
          buttonStyle={styles.button}
          titleStyle={styles.titleButton}
          icon={{
            type: 'material-community',
            name: 'square-edit-outline',
            color: colors.PRIMARY_COLOR,
          }}
          onPress={() => navigation.navigate('add-review-restaurant', { idRestaurant })}
        />
      ) : (
        <Button
          title='Inicia sesión para escribir un comentario'
          buttonStyle={styles.button}
          titleStyle={styles.titleButton}
          onPress={() => navigation.navigate('account-stack', { screen: 'login' })}
          icon={{
            type: 'material-community',
            name: 'account-circle-outline',
            color: colors.PRIMARY_COLOR,
          }}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  button: { padding: 20, backgroundColor: 'transparent' },
  titleButton: { color: colors.PRIMARY_COLOR },
});
