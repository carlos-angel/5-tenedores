import { View, Text, StyleSheet, ActivityIndicator, FlatList } from 'react-native';
import { Button } from 'react-native-elements';
import { map, size } from 'lodash';
import { useFocusEffect } from '@react-navigation/native';
import { useState, useCallback } from 'react';
import { getReviewByIdRestaurantAndLimit } from 'services/reviews/get-reviews';
import ReviewItem from 'components/Review/ReviewItem';
import colors from 'constants/colors';

export default function Reviews({ idRestaurant }) {
  const [reviews, setReviews] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isMoreReviews, setIsMoreReviews] = useState(true);
  const limitReviews = 10;
  const sizeReviews = size(reviews);

  useFocusEffect(
    useCallback(() => {
      (async () => await moreReviews())();
    }, []),
  );

  const moreReviews = () => {
    const startAfterCreateAt = sizeReviews > 0 ? reviews[sizeReviews - 1].createAt : '';
    setLoading(true);

    getReviewByIdRestaurantAndLimit({ idRestaurant, startAfterCreateAt, limitReviews })
      .then((newReviews) => {
        setReviews([...reviews, ...newReviews]);
        size(newReviews) !== limitReviews && setIsMoreReviews(false);
      })
      .catch(() => setReviews(reviews))
      .finally(() => setLoading(false));
  };

  return (
    <View>
      {sizeReviews > 0 && map(reviews, (comment) => <ReviewItem key={comment.id} {...comment} />)}

      {loading && (
        <View style={styles.loadingComments}>
          <ActivityIndicator size='large' color={colors.PRIMARY_COLOR} />
        </View>
      )}

      {isMoreReviews ? (
        <Button
          title='obtener más comentarios'
          buttonStyle={styles.button}
          titleStyle={styles.title}
          onPress={moreReviews}
        />
      ) : (
        <View style={styles.notMoreReviews}>
          <Text style={{ color: '#9CA3AF' }}>No hay más comentarios</Text>
        </View>
      )}

      {sizeReviews < 1 && (
        <View style={styles.notComments}>
          <Text style={{ color: '#9CA3AF' }}>Se el primero en comentar este restaurante</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  button: {
    padding: 20,
    backgroundColor: 'transparent',
  },
  title: {
    color: colors.PRIMARY_COLOR,
  },
  descriptionNotAuth: {
    textAlign: 'center',
    color: colors.PRIMARY_COLOR,
    padding: 20,
  },
  loadingComments: {
    marginTop: 10,
    marginLeft: 10,
    alignItems: 'center',
  },
  notMoreReviews: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: 'center',
  },
  notComments: {
    padding: 10,
    paddingTop: 5,
    alignItems: 'center',
  },
});
