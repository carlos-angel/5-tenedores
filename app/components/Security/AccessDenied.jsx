import { StyleSheet, Text, View } from 'react-native';
import { Icon, Button } from 'react-native-elements';
import colors from 'constants/colors';

export default function AccessDenied({ navigation, message }) {
  return (
    <View style={styles.viewBody}>
      <Icon type='material-community' name='alert-outline' size={50} />
      <Text style={styles.message}>{message}</Text>
      <Button
        title='Iniciar sesión'
        buttonStyle={styles.buttonLogin}
        titleStyle={styles.titleButtonLogin}
        containerStyle={styles.buttonLoginContainer}
        onPress={() => navigation.navigate('account-stack', { screen: 'login' })}
        icon={{
          type: 'material-community',
          name: 'account-circle-outline',
          color: colors.BACKGROUND,
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  viewBody: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  message: { fontSize: 18, fontWeight: 'bold', textAlign: 'center' },
  buttonLoginContainer: { marginTop: 20, width: '80%' },
  buttonLogin: { backgroundColor: colors.PRIMARY_COLOR },
  titleButtonLogin: { color: colors.BACKGROUND },
});
