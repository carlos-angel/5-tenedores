import { View, StyleSheet } from 'react-native';
import { ListItem, Icon } from 'react-native-elements';
import React, { useState } from 'react';
import { map } from 'lodash';
import { useAccount } from 'hooks/useAccount';
import menuOptions from 'utils/menuOptions';
import Modal from 'components/Shared/Modal';
import accountOptionsForms from 'utils/accountOptionsForms';
import { getValidationSchema } from 'utils/getValidationSchema';
import { getInitialValues } from 'utils/getInitialValues';
import AccountForm from 'components/Account/AccountForm';
import colors from 'constants/colors';

export default function AccountOptions() {
  const { user, setReloadUser } = useAccount();
  const [showModal, setShowModal] = useState(false);
  const [selectedForm, setSelectedForm] = useState({
    initialValues: {},
    validationSchema: {},
    inputs: {},
    isReauthenticate: false,
    onSubmit: () => {},
  });
  if (!user) return null;

  const onPress = (key) => {
    const form = accountOptionsForms.find(({ form }) => form === key);
    const validationSchema = getValidationSchema(form.inputs);

    const { displayName, email } = user;
    const valuesForms = [
      {
        form: 'displayName',
        values: { displayName },
      },
      {
        form: 'email',
        values: { email },
      },
      {
        form: 'password',
        values: {},
      },
    ];

    const valuesForm = valuesForms.find(({ form }) => form === key);
    const initialValues = getInitialValues(form.inputs, valuesForm.values);
    setSelectedForm({
      initialValues,
      validationSchema,
      inputs: form.inputs,
      isReauthenticate: form.isReauthenticate,
      onSubmit: form.onSubmit,
    });
    setShowModal(true);
  };

  return (
    <View>
      {map(menuOptions, (menu, index) => (
        <ListItem key={index} containerStyle={styles.menuItem} onPress={() => onPress(menu.key)}>
          <Icon
            type={menu.iconType}
            name={menu.iconNameLeft}
            size={22}
            color={menu.iconColorLeft}
          />
          <ListItem.Content>
            <ListItem.Title style={{ color: colors.TERTIARY_COLOR }}>{menu.title}</ListItem.Title>
          </ListItem.Content>
          <Icon
            type={menu.iconType}
            name={menu.iconNameRight}
            size={22}
            color={menu.iconColorRight}
          />
        </ListItem>
      ))}
      {showModal && (
        <Modal isVisible={showModal} setIsVisible={setShowModal}>
          <AccountForm
            setReloadUser={setReloadUser}
            {...selectedForm}
            setShowModal={setShowModal}
          />
        </Modal>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  menuItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
  },
});
