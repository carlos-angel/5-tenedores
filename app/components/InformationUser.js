import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import Toast from 'react-native-toast-message';
import { useAccount } from 'hooks/useAccount';
import { getImage } from 'utils/getImage';
import AvatarNotFound from 'assets/img/avatar-default.jpg';
import { uploadImageService } from 'services/files/image.service';
import { getAvatarService } from 'services/account/get-avatar.service';
import { updateUserService } from 'services/account/update-user.service';
import colors from 'constants/colors';

export default function InformationUser() {
  const { user, setReloadUser, setLoading } = useAccount();
  if (!user) return null;
  const { photoURL, displayName, email, uid } = user;

  const changeAvatar = async () => {
    const { error, uri } = await getImage();

    if (!error && uri) {
      setLoading({ text: 'subiendo imagen', isVisible: true });
      const isUploadImage = await uploadImageService(uri, 'avatar', uid);

      if (!isUploadImage) {
        return Toast.show({ type: 'error', text1: 'Error al subir la imagen' });
      }
      const photoURL = await getAvatarService(uid);

      const result = await updateUserService({ photoURL });
      setLoading({ text: '', isVisible: false });
      result && setReloadUser(true);
      result && Toast.show({ type: 'success', text1: 'imagen subida' });
    }
  };

  return (
    <View style={styles.viewInformation}>
      <Avatar
        rounded
        size='large'
        containerStyle={styles.avatar}
        source={photoURL ? { uri: photoURL } : AvatarNotFound}
      >
        <Avatar.Accessory size={26} onPress={changeAvatar} />
      </Avatar>
      <View>
        <Text style={styles.displayName}>{displayName}</Text>
        <Text style={{ color: colors.TERTIARY_COLOR }}>{email}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewInformation: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    paddingTop: 30,
    paddingBottom: 30,
  },
  avatar: {
    marginRight: 20,
    backgroundColor: '#bcbec1',
  },
  displayName: {
    fontWeight: 'bold',
    paddingBottom: 5,
    color: colors.TERTIARY_COLOR,
  },
});
