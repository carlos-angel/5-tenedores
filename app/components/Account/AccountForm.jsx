import { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import { Formik } from 'formik';
import Toast from 'react-native-toast-message';
import * as Yup from 'yup';
import { map } from 'lodash';
import Input from 'components/Form/Input';
import { reauthenticateService } from 'services/auth/reauthenticate.service';
import colors from 'constants/colors';

export default function AccountForm(props) {
  const {
    initialValues,
    validationSchema,
    onSubmit,
    inputs,
    isReauthenticate,
    setReloadUser,
    setShowModal,
  } = props;
  const [errorAuthenticated, setErrorAuthenticated] = useState('');
  return (
    <View style={styles.view}>
      <Formik
        initialValues={initialValues}
        validationSchema={Yup.object(validationSchema)}
        onSubmit={async (values) => {
          setErrorAuthenticated('');
          if (isReauthenticate) {
            const { password } = values;
            const reauthenticate = await reauthenticateService(password);
            if (!reauthenticate) {
              setErrorAuthenticated('la contraseña es incorrecta');
              return false;
            }
          }
          const { error, message } = await onSubmit(values);
          const isCloseModal = error;
          if (!error) {
            setReloadUser(true);
            Toast.show({ type: 'success', text1: message });
          }
          setShowModal(isCloseModal);
        }}
      >
        {({
          handleChange,
          handleSubmit,
          setFieldTouched,
          errors,
          isSubmitting,
          touched,
          values,
        }) => (
          <>
            {map(inputs, (input) => {
              return (
                <Input
                  key={input.name}
                  placeholder={input.placeholder}
                  defaultValue={values[input.name]}
                  containerStyle={styles.input}
                  secureTextEntry={input.secureTextEntry}
                  iconName={input.iconName}
                  iconStyle={styles.iconRight}
                  onChangeText={handleChange(input.name)}
                  onBlur={() => setFieldTouched(input.name, true, true)}
                  errorMessage={touched[input.name] && errors[input.name] ? errors[input.name] : ''}
                />
              );
            })}
            {!!isReauthenticate && <Text style={styles.error}>{errorAuthenticated}</Text>}
            <Button
              title='Actualizar'
              onPress={handleSubmit}
              containerStyle={styles.containerButton}
              buttonStyle={styles.button}
              loading={isSubmitting}
              disabled={isSubmitting}
            />
          </>
        )}
      </Formik>
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  containerButton: {
    width: '95%',
  },
  button: {
    backgroundColor: colors.PRIMARY_COLOR,
  },
  input: {
    marginBottom: 10,
    color: colors.TERTIARY_COLOR,
  },
  iconRight: {
    color: colors.SECONDARY_COLOR,
  },
  error: {
    color: 'red',
    paddingBottom: 5,
  },
});
