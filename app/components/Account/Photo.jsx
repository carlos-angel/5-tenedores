import { View, Dimensions, StyleSheet } from 'react-native';
import { Image } from 'react-native-elements';

const widthScreen = Dimensions.get('window').width;

export default function Photo({ uri }) {
  return (
    <View style={styles.viewPhoto}>
      <Image
        source={uri ? { uri } : require('assets/no-image.png')}
        style={{ width: widthScreen, height: 200 }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  viewPhoto: {
    alignItems: 'center',
    height: 200,
    marginBottom: 20,
  },
});
