module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      'inline-dotenv',
      [
        'module-resolver',
        {
          alias: {
            assets: './assets',
            components: './app/components',
            constants: './app/constants',
            context: './app/context',
            hooks: './app/hooks',
            navigations: './app/navigations',
            screens: './app/screens',
            services: './app/services',
            utils: './app/utils',
          },
        },
      ],
    ],
  };
};
