# 5 Tenedores

Plataforma para que los usuarios compartan sus experiencias con todo el mundo acerca de los restaurantes visitados durante sus viajes.
App realizado con React Native Expo y firebase

## Requirements

- instalar un emulador android, ios o un dispositivo físico con la App de Expo.

## Getting started

```bash
# install dependencies
yarn install

# copy environment variables file
cp .env.example .env

# run App
yarn start

#run App Android
yarn android

#run App IOS
yarn ios
```
